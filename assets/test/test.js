QUnit.test("Here's a test that should always pass", function (assert) {
    assert.ok(1 <= "3", "1<3 - the first agrument is 'truthy', so we pass!");
});
QUnit.test('Testing compute Function with several inputs', function (assert) {
    assert.equal(perimeter(4,4), 16, 'Tested with two positive integers');
   assert.equal(area(7,1), 7,'Tested with two negative integers');
    assert.equal(tan(180), 1.3386902103511544,'For trignometry for tan '); 
    assert.equal(cos(3), -0.9899924966004454,'For trignometry for cos 3');  
  
});

