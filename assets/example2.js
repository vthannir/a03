var firstNum=firstNum();
var secondNum=secondNum();
var setValue=setValue();


function setValue(){
    var set = document.getElementById('opList').value;
    return set;
}

function firstNum(){
    return parseInt(document.getElementById('firstNum').value);
}

function secondNum(){
    return parseInt(document.getElementById('secondNum').value);
}

function max(first,second){
    if(first>second)
        return first;
    else
        return second;
}

function min(first, second){
    if(first<second)
        return first;
    else return second;
}

function add(first, second){
    let add =0;
    if(first<second){
        for(let i =first;i<=second;i++)
            add += i;
    }
    else
        for(let i = second;i<=first;i++)
        add += i;
    return add;
}

function mul(first, second){
    let mul =1;
    if(first<second){
        for(let i =first;i<=second;i++)
            mul *= i;
    }
    else
        for(let i = second;i<=first;i++)
        mul *= i;
    return mul;
}

function handler(){
    let result = "";
    if(setValue === "Max")
        result = max(firstNum,secondNum);
    else if(setValue === "Min")
        result = min(firstNum,secondNum);
    else if(setValue === "Add")
        result = add(firstNum,secondNum);
    else
        result = mul(firstNum,secondNum);
    
    document.getElementById('result').value = result;
}