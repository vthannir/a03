var firstNum;
var secondNum;
var selectValue = setValue();

function handleClick() {
    firstNum=parseInt(document.getElementById("firstNum").value);
    secondNum=parseInt(document.getElementById("secondNum").value);
    var result;

if(selectValue === "Perimeter") {
    result = perimeter(firstNum,secondNum);
} else if(selectValue == "Area") {
    result = area(firstNum,secondNum);
} else if(selectValue == "PS") {
    result = tan(firstNum);
} else if(selectValue == "AS")  {
    result = cos(firstNum);
}

var output=document.getElementById("result");
output.value=result;
}

function perimeter(firstNum,secondNum){
    return parseInt((firstNum+secondNum)*2);
}
function area(firstNum,secondNum){
    return parseInt(firstNum*secondNum);
}
function tan(firstNum){
    return Math.tan(firstNum);
}
function cos(firstNum){
    return Math.cos(firstNum);
}

function setValue() {
    selectValue=document.getElementById("opList").value;
}

